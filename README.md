# Exercice React & API - Création d'un site d'informations sur les sorciers de Harry Potter

## Objectif
Créer un site en React qui utilise l'API [Harry Potter](https://hp-api.onrender.com) pour afficher les 20 premiers sorciers, mettre en place un champ de recherche avec autocomplétion pour trouver un sorcier par son nom, et afficher une fiche détaillée avec au moins 8 informations lorsque l'on clique sur un sorcier.

## Prérequis
Assurez-vous d'avoir Node.js installé sur votre machine.    
L'utilisation de cette API ne requiert aucune authentification.

## Étapes

### Étape 1: Initialisation du projet React
1. Ouvrez votre terminal et créez un nouveau projet React avec la commande suivante :
   ```bash
   npx create-react-app sorciers-hp

2. Accédez au répertoire du projet :
   ```bash 
   cd sorciers-hp

3. Lancez l'application pour vous assurer que tout fonctionne correctement :
   ```bash
   npm start

### Étape 2: Installation des dépendances
1. Installez Axios pour effectuer des requêtes HTTP :
   ```bash
   npm install axios

2. Installez la bibliothèque graphique de votre choix (par exemple, Material-UI, Ant Design, Bootstrap, etc.).

### Étape 3: Création des composants
Créez un dossier components à l'intérieur du dossier src.

Dans le dossier components, créez les composants suivants :

- WizardsList : Liste des 20 premiers sorciers.
- SearchWizard : Champ de recherche avec autocomplétion.
- WizardDetails : Fiche détaillée du sorcier.


### Étape 4: Intégration de l'API
Dans WizardsList, récupérez les 20 premiers sorciers de l'API.

Affichez ces sorciers sous forme de liste en utilisant les composants de votre bibliothèque graphique choisie.

### Étape 5: Mise en place de la recherche
Dans SearchWizard, utilisez le composant de recherche avec autocomplétion fourni par votre bibliothèque graphique.

Lorsqu'un sorcier est sélectionné, mettez à jour la liste affichée dans WizardList.

### Étape 6: Affichage détaillé
Dans WizardDetails, affichez au moins 8 informations du sorcier sélectionné en utilisant les composants de votre bibliothèque graphique.

Utilisez ce composant pour afficher les détails lorsque l'on clique sur un sorcier dans la liste des 20 premiers sorciers ou dans les résultats de la recherche.

### Étape 7: Intégration de tous les composants
Dans App.js, intégrez les composants WizardsList, SearchWizard, et WizardDetails.
Vous pouvez également utiliser un page spécifique qui sera incluse dans votre fichier App.js.

Assurez-vous que le site fonctionne correctement en testant chaque fonctionnalités.

### Étape 8: Styles (bonus)
Ajoutez des styles supplémentaires avec les fonctionnalités fournies par votre bibliothèque graphique si vous le souhaitez.

### Remarques
- Afin de répondre aux meilleurs pratiques, délocalisez les appels API dans un dossier API afin de n'avoir qu'un appel de fonction dans vos composants.   
- N'oubliez pas de gérer les erreurs potentielles lors de l'appel à l'API.    
- Vous avez le droit d'ajouter des fonctionnalités au projet (loader, tests, TypeScript etc...)
- Utilisez les composants de manière modulaire et réutilisable.   
- Commentez votre code pour expliquer votre démarche.


### Ressources
Documentation React : https://reactjs.org/docs/getting-started.html  
Documentation Axios : https://axios-http.com/docs/intro     
Lien vers l'API Harry Potter [https://hp-api.onrender.com](https://hp-api.onrender.com)     
Documentation de votre bibliothèque graphique choisie : (libre de choix)

### Soumission
Lorsque vous avez terminé, assurez-vous de fournir le code source du projet pour évaluation.    
Le projet doit être testable dans un navigateur sur le port 3000.
